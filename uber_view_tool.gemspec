lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "uber_view_tool/version"

Gem::Specification.new do |spec|
  spec.name          = "uber_view_tool"
  spec.version       = UberViewTool::VERSION
  spec.authors       = ["ryanjames11"]
  spec.email         = ["admin@ryanaldred.ca"]

  spec.summary       = %q{A pretty uber view tool}
  spec.description   = %q{Set some uber things in the view}
  spec.homepage      = "https://ryanaldred.ca"
  spec.license       = "MIT"

  spec.metadata["allowed_push_host"] = "TODO: Set to 'https://rubygems.org'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/ryanjames11/uber_view_tool"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/ryanjames11/uber_view_tool"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
